import AppLayout from '@/components/Layouts/AppLayout'
import { useState, useEffect } from "react";
import Head from 'next/head';
import axios from '@/lib/axios'
import { useRouter } from "next/router";
import Swal from 'sweetalert2'
import { Select } from 'antd';



const EmployeeCreate = () => {
   
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [company, setCompany] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
  
    const [companies, setCompanies] = useState([]);


    const [errors, setErrors] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
  
    const { Option } = Select;

    useEffect(() => {
        fetchCompanies()
      }, [])

    const handleSubmit = (event) => {
        event.preventDefault();

        if (isLoading) return;

        setErrors([]);
        setIsLoading(true);

        let recordData = new FormData();
        recordData.append('first_name', firstName);
        recordData.append('last_name', lastName);
        recordData.append('email', email);
        recordData.append('phone', phone);
        recordData.append('company', company);

      

        axios.post('/api/employees/', recordData)
            .then(response => {
                Swal.fire({
                    icon: 'success',
                    title: 'Employee added successfully!'
                })
                router.push('/employee')
            })
            .catch(error => {
                setErrors(error.response.data.errors);
                setIsLoading(false);
                Swal.fire({
                    icon: 'error',
                    title: 'Error. Please try again'
                })
            })
            .finally(() => setIsLoading(false))

    }

    const fetchCompanies = async () => {

        await axios
          .get("/api/companies")
          .then(response => {
            setCompanies(response.data)
            setIsLoading(false)
          });
      };
    

    const errorMessage = (field) => {
        return (
          <div className="text-red-600 mt-1">
            {errors?.[field]?.map((msg, index) => {
              return (
                <div key={index}>{msg}</div>
              )
            })}
          </div>
        )
      }
   
    return (
        <AppLayout
          header={
            <h2 className="font-semibold text-xl text-gray-800 leading-tight">
              Create New Employee
            </h2>
          }>
    
          <Head>
            <title>CareMaster - Dashboard | New Employee</title>
          </Head>
    
    
          <div className="py-12">
            <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
    
              <div className="p-6 bg-white border-b border-gray-200">
                {isLoading ?
                  <div><p>...Loading</p></div>
                  :
                  <form >
    
                    <div className="flex flex-wrap overflow-hidden xl:-mx-3">
                      <div className="w-full overflow-hidden xl:my-3 xl:px-3 xl:w-full">
                        <div>
                          <label
                            htmlFor="companies"
                            className="block font-medium text-sm text-gray-700"
                          >
                            Company
                          </label>
                          <select
                            value={company}
                            onChange={(e) => setCompany(e.target.value)}
                            id="companies"
                            className="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                          >
                            {
                              companies?.data?.map(res => (
    
                                <option
                                  value={res.id}
                                  key={res.id}>{res.name}</option>
                              ))
                            }
    
                          </select>
                          {errorMessage('company')}
    
                        </div>
                      </div>
                    </div>
    
    
                    <div className="flex flex-wrap overflow-hidden xl:-mx-3">
                      <div className="w-full overflow-hidden xl:my-3 xl:px-3 xl:w-1/2">
                        <div>
                          <label
                            htmlFor="first_name"
                            className="block font-medium text-sm text-gray-700">
                            First Name
                          </label>
                          <input
                            value={firstName}
                            onChange={(e) => setFirstName(e.target.value)}
                            id="first_name"
                            type="text"
                            className="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                          />
                        </div>
                        {errorMessage('first_name')}
                      </div>
    
                      <div className="w-full  overflow-hidden xl:my-3 xl:px-3 xl:w-1/2">
                        <div >
                          <label
                            htmlFor="last_night"
                            className="block font-medium text-sm text-gray-700">
                            Last Name
                          </label>
                          <input
                            value={lastName}
                            onChange={(e) => setLastName(e.target.value)}
                            id="last_night"
                            type="text"
                            className="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                          />
                        </div>
                        {errorMessage('email')}
                      </div>
    
    
    
    
    
    
    
    
                    </div>
    
    
    
    
                    <div className="flex flex-wrap overflow-hidden xl:-mx-3">
                      <div className="w-full overflow-hidden xl:my-3 xl:px-3 xl:w-1/2">
                        <div>
                          <label
                            htmlFor="phone"
                            className="block font-medium text-sm text-gray-700">
                            Phone
                          </label>
                          <input
                            value={phone}
                            onChange={(e) => setPhone(e.target.value)}
                            id="phone"
                            type="text"
                            className="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                          />
                        </div>
                        {errorMessage('phone')}
                      </div>
    
                      <div className="w-full overflow-hidden xl:my-3 xl:px-3 xl:w-1/2">
                        <div>
                          <label
                            htmlFor="email"
                            className="block font-medium text-sm text-gray-700">
                            Email
                          </label>
                          <input
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            id="name"
                            type="email"
                            className="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                          />
                        </div>
                        {errorMessage('email')}
                      </div>
    
    
    
    
                    </div>
    
    
    
    
    
                    <div className="mt-4">
                      <div className="flex justify-between">
                     
    
    
                        <button onClick={handleSubmit}
                          type="submit" className="flex items-center px-3 py-2 bg-blue-600 text-white rounded" disabled={isLoading}>
                          <svg role="status" className={`w-4 h-4 mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-blue-600 inline ${!isLoading ? 'hidden' : ''}`} viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor" />
                            <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill" />
                          </svg>
                          <span>Save</span>
                        </button>
    
    
                      </div>
                    </div>
    
    
    
                  </form>
                }
              </div>
            </div>
    
          </div>
        </AppLayout >
    
    
    
    
      );
}

export default EmployeeCreate