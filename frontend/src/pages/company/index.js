import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import AppLayout from '@/components/Layouts/AppLayout'
import Head from 'next/head';
import axios from '@/lib/axios'
import Link from 'next/link'


const CompanyIndex = () => {
  const router = useRouter();

  const [records, setRecords] = useState([]);
  const [loading, setLoading] = useState(false)

  const [queryPage, setQueryPage] = useState(1);
  const [queryName, setQueryName] = useState('');
  const [queryOrderColumn, setQueryOrderColumn] = useState('id')
  const [queryOrderDirection, setQueryOrderDirection] = useState('desc');


  
  useEffect(() => {
    setLoading(true)
    fetchRecords()
}, [])


useEffect(() => {
    fetchRecords()
}, [queryPage, queryName,  queryOrderColumn, queryOrderDirection])

const fetchRecords = async () => {

  await axios
      .get("/api/companies", {
          params: {
              page: queryPage,
              order_column: queryOrderColumn,
              order_direction: queryOrderDirection,
              name: queryName
          }
      })
      .then(response => {
          setRecords(response.data)
          setLoading(false)
      });
};


const handleNameFilterChange = (event) => {
  setQueryPage(1);
  setQueryName(event.target.value);

}


    // PAGINATION
    const pageChanged = (url) => {
      const fullUrl = new URL(url);
      setQueryPage(fullUrl.searchParams.get('page'))
  }

  const orderColumnIcon = (column) => {
      let icon = 'fa-sort';

      if (queryOrderColumn === column) {
          if (queryOrderDirection === 'asc') {
              icon = 'fa-sort-up'
          } else {
              icon = 'fa-sort-down'
          }
      }

      return (
          <i className={`fa-solid ${icon}`}></i>
      )
  }

  const updateOrderColumn = (column) => {
      let direction = 'asc';

      if (column === queryOrderColumn) {
          direction = queryOrderDirection === 'asc' ? 'desc' : 'asc'
      }

      setQueryPage(1);
      setQueryOrderColumn(column)
      setQueryOrderDirection(direction)
  }



    // COMPONENTS
  

  function RenderNameFilter() {
      return (
          <div className="mx-2">
              <input
                  placeholder="Search"
                  type="text"
                  onChange={(e) => handleNameFilterChange(e)}
                  value={queryName}
                  className="block w-full rounded-md  focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" />

          </div>
      )
  }

  function RenderPagination(params) {
      return (
          <nav
              role="navigation"
              aria-label="Pagination Navigation"
              className="flex items-center justify-between"
          >

              <div className="hidden sm:flex-1 sm:flex sm:items-center sm:justify-between">
                  <div>
                      <p className="text-sm text-gray-700 leading-5">
                          Showing
                          <span>
                              <span className="font-medium">
                                  {" "}
                                  {records?.meta?.from}{" "}
                              </span>
                              to
                              <span className="font-medium">
                                  {" "}
                                  {records?.meta?.to}{" "}
                              </span>
                          </span>
                          of
                          <span className="font-medium">
                              {" "}
                              {records?.meta?.total}{" "}
                          </span>
                          results
                      </p>
                  </div>

                  <div>
                      <span className="relative z-0 inline-flex shadow-sm rounded-md">

                          <RenderPaginationLinks />

                      </span>
                  </div>
              </div>

          </nav>
      );
  }

  function RenderPaginationLinks(params) {
      return (

          <div>
              {
                  records?.meta?.links &&
                  <span>
                      {records.meta.links.map((link, index) => (
                          <button key={index}
                              dangerouslySetInnerHTML={{ __html: link.label }}
                              onClick={() => pageChanged(link.url)}
                              className="relative inline-flex items-center px-4 py-2 -ml-px text-sm font-medium text-gray-700 bg-white border
                          border-gray-300 leading-5 hover:text-gray-500 focus:z-10 focus:outline-none focus:ring ring-gray-300 focus:border-blue-300
                           active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150 first:rounded-l-md last:rounded-r-md"
                          />

                      ))}

                  </span>
              }

          </div>

      )
  }











  return (
    <AppLayout
        header={
            <h2 className="font-semibold text-xl text-gray-800 leading-tight">
                All Companies
            </h2>
        }>

        <Head>
            <title>CareMaster | Companies</title>
        </Head>


        <div className="py-12">

            <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg">


                    <div className="min-w-full align-middle">
                        <div className="m-4 flex justify-between ">
                            {RenderNameFilter()}
                            <button className="bg-blue-700 p-2 text-white rounded"
                                onClick={() => router.push('/company/create')}>
                                Add New Company
                            </button>

                        </div>
                    </div>



                    <div className="p-6 bg-white border-b border-gray-200">
                        {loading ?
                            <div><p>...LOADING</p></div>
                            :
                            <div>
                                <table className="table">
                                    <thead className="table-header">
                                        <tr>

                                            <th>
                                                <div >
                                                    <span>Logo</span>
                                                    <button
                                                        type="button"
                                                        className="column-sort">
                                                    </button>
                                                </div>
                                            </th>
                                            <th>
                                                <div >
                                                    <span>Name</span>
                                                    <button
                                                        onClick={() => updateOrderColumn('name')}
                                                        type="button"
                                                        className="column-sort">
                                                        {orderColumnIcon('name')}
                                                    </button>
                                                </div>
                                            </th>

                                            <th>
                                                <div >
                                                    <span>Email</span>
                                                    <button
                                                        onClick={() => updateOrderColumn('email')}
                                                        type="button"
                                                        className="column-sort">
                                                        {orderColumnIcon('email')}
                                                    </button>
                                                </div>
                                            </th>

                                            <th>
                                                <div >
                                                    <span>Website</span>
                                                    <button
                                                        onClick={() => updateOrderColumn('website')}
                                                        type="button"
                                                        className="column-sort">
                                                        {orderColumnIcon('website')}
                                                    </button>
                                                </div>
                                            </th>
                                          
                                           

                                        

                                            <th>
                                                <div>
                                                    <span className="pr-6">Created at</span>
                                                    <button
                                                        onClick={() => updateOrderColumn('created_at')}
                                                        type="button"
                                                        className="column-sort">
                                                        {orderColumnIcon('created_at')}
                                                    </button>
                                                </div>
                                            </th>
                                            <th></th>
                                        </tr>

                                    </thead>

                                    <tbody className="table-body">


                                        {records?.data?.map(rec => (
                                            <tr key={rec.id}>
                                                <td>  <img className="object-contain h-32 w-32"
                                                    src={rec.logo} />                                                       </td>
                                                <td>{rec.name}</td>
                                                <td>{rec.email}</td>
                                                <td>{rec.website}</td>
                                                <td>{rec.created_at}</td>
                                                <td><Link href={`/company/${rec.id}`}>Edit</Link></td>
                                            </tr>
                                        ))}
                                    </tbody>

                                </table>

                                <div className="mt-4">
                                    <RenderPagination />
                                </div>
                            </div>
                        }

                    </div>

                </div>
            </div>


        </div>





    </AppLayout>
)
}

export default CompanyIndex