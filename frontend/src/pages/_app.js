import 'tailwindcss/tailwind.css'
import '../styles/globals.css'
import "antd/dist/antd.min.css";


const App = ({ Component, pageProps }) => <Component {...pageProps} />

export default App
