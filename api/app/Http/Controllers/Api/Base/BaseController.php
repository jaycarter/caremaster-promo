<?php

namespace App\Http\Controllers\Api\Base;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class BaseController extends Controller
{
    protected function deleteImage($imagePath)
    {
        $exists = Storage::disk('public')->exists($imagePath);
        if ($exists) {
            Storage::disk('public')->delete($imagePath);
        }
    }
}
