<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyRequest;
use App\Http\Resources\CompanyResource;
use Illuminate\Support\Facades\Notification;
use App\Notifications\NewCompanyNotification;
use App\Http\Controllers\Api\Base\BaseController;

class CompanyController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $orderColumn = $request->input('order_column', 'id');
        $orderDirection = $request->input('order_direction', 'desc');

        if (!in_array($orderColumn, ['id', 'name', 'created_at'])) {
            $orderColumn = 'id';
        }

        if (!in_array($orderDirection, ['asc', 'desc'])) {
            $orderDirection = 'desc';
        }

        $companies = Company::query()
            ->when($request->name, fn ($query, $name) => $query->where('name', 'like', '%' . $name . '%'))
            ->orderBy($orderColumn, $orderDirection)
            ->paginate(10);


        return CompanyResource::collection($companies);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {
        $company = Company::create($request->validated());
        $user = User::first();


        if ($request->hasFile('logo')) {

            //   remove old image
            if ($company->logo) {
                $this->deleteImage($company->logo);
            }

            $this->validate($request, [
                'logo' => 'nullable|image|mimes:jpeg,png,jpg|dimensions:min_width=100,min_height=100'
            ]);

            $logo = $request->file('logo');
            $fileName = time() . '-' . $logo->getClientOriginalName();
            $path = $logo->storeAs(COMPANY::IMAGE_URL, $fileName);
            $company->logo = $path;
            $company->save();
        }


        Notification::send($user, new NewCompanyNotification($company));

        return new CompanyResource($company);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        return new CompanyResource($company);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyRequest $request, Company $company)
    {
        $company->update($request->validated());

        if ($request->hasFile('logo')) {

            //   remove old image
            if ($company->logo) {
                $this->deleteImage($company->logo);
            }

            $this->validate($request, [
                'logo' => 'nullable|image|mimes:jpeg,png,jpg|dimensions:min_width=100,min_height=100'
            ]);

            $logo = $request->file('logo');
            $fileName = time() . '-' . $logo->getClientOriginalName();
            $path = $logo->storeAs(COMPANY::IMAGE_URL, $fileName);
            $company->logo = $path;
            $company->save();
        }

        return new CompanyResource($company);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $company->delete();
        return response()->noContent();
    }
}
