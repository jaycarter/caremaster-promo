<?php

namespace App\Models;

use App\Models\Company;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Employee extends Model
{
    use HasFactory;

    public $fillable = ['first_name', 'last_name', 'company', 'email', 'phone'];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

}
