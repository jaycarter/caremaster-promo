<?php

namespace App\Models;

use App\Models\Employee;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Company extends Model
{
    use HasFactory;
    use Notifiable;


    const IMAGE_URL = 'images/companies';


    public $fillable = ['name', 'website', 'logo', 'email'];

    protected function logo(): Attribute
    {

       $default = "https://www.ghardhudho.com/admin/public/uploads/company_logo/default-cmpny.jpg";

        $url = url('/');
        return  Attribute::make(
            get: fn ($value) => $value == null ? $default : $url . '/storage/' . $value,
        );
    }


    public function employees()
    {
        return $this->hasMany(Employee::class);
    }
}
