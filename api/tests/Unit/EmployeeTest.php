<?php

namespace Tests\Unit;

use Faker\Factory;
use Tests\TestCase;
use App\Models\User;
use Faker\Generator;
use App\Models\Company;
use App\Models\Employee;
use Illuminate\Http\Response;
use Tests\CreatesApplication;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class EmployeeTest extends TestCase
{

    use CreatesApplication, DatabaseMigrations;

    private Generator $faker;

    public function setUp(): void
    {

        parent::setUp();
        $this->faker = Factory::create();
        // Artisan::call('migrate:refresh');
    }


    public function testEmployeeIsCreatedSuccessfully()
    {
        User::factory()->count(1)->create();
        Company::factory()->count(1)->create();

        $user = User::first();
        $this->actingAs($user, 'web');

        $payload = [
            'first_name' => $this->faker->firstName,
            'last_name'  => $this->faker->lastName,
            'email'      => $this->faker->email,
            'company' => 1,
            'phone' => '6435729'
        ];



        $this->json('post', 'api/employees', $payload)
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'first_name',
                        'last_name',
                        'email',
                        'phone',
                        'created_at',

                    ]
                ]
            );
        $this->assertDatabaseHas('employees', $payload);
    }

    public function testEmployeeIsShownCorrectly()
    {
        User::factory()->count(1)->create();
        Company::factory()->count(1)->create();

        $user = User::first();
        $this->actingAs($user, 'web');

        $employee = Employee::create(
            [
                'first_name' => $this->faker->firstName,
                'last_name'  => $this->faker->lastName,
                'email'      => $this->faker->email,
                'company' => 1,
                'phone' => '6435729'
            ]
        );


        $this->json('get', "api/employees/" . $employee->id)
            ->assertStatus(200)
            ->assertJson(
                [
                    'data' => [
                        'id'         => $employee->id,
                        'company'    => $employee->company,
                        'first_name' => $employee->first_name,
                        'last_name'  => $employee->last_name,
                        'email'      => $employee->email,
                        'phone'      => $employee->phone,

                    ]
                ]
            );
    }


    public function testUpdateEmployeeReturnsCorrectData() {

        User::factory()->count(1)->create();
        Company::factory()->count(1)->create();

        $user = User::first();
        $this->actingAs($user, 'web');

        $employee = Employee::create(
            [
                'first_name' => $this->faker->firstName,
                'last_name'  => $this->faker->lastName,
                'email'      => $this->faker->email,
                'company' => 1,
                'phone' => '6435729'
            ]
        );


        $payload = [
            'first_name' => $this->faker->firstName,
            'last_name'  => $this->faker->lastName,
            'email'      => $this->faker->email,
            'company' => 1

        ];


        $this->json('put', "api/employees/$employee->id", $payload)
            ->assertStatus(200)
            ->assertJson(
                [
                    'data' => [
                        'id'         => $employee->id,
                        'first_name' => $payload['first_name'],
                        'last_name'  => $payload['last_name'],
                        'email'      => $payload['email'],

                    ]
                ]
            );
    }

    public function testEmployeeIsDestroyed() {

        User::factory()->count(1)->create();
        Company::factory()->count(1)->create();

        $user = User::first();
        $this->actingAs($user, 'web');

        $data =
            [
                'first_name' => $this->faker->firstName,
                'last_name'  => $this->faker->lastName,
                'email'      => $this->faker->email,
                'company' => 1,
                'phone' => '6435729'
            ];

            $employee = Employee::create(
            $data
        );

        $this->json('delete', "api/employees/$employee->id")
             ->assertNoContent();
        $this->assertDatabaseMissing('employees', $data);
    }
}
