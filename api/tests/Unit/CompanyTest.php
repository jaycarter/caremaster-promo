<?php

namespace Tests\Unit;

use Faker\Factory;
use Tests\TestCase;
use App\Models\User;
use Faker\Generator;
use App\Models\Company;
use Illuminate\Http\Response;
use Tests\CreatesApplication;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CompanyTest extends TestCase
{


    use CreatesApplication, DatabaseMigrations;

    private Generator $faker;

    public function setUp(): void
    {

        parent::setUp();
        $this->faker = Factory::create();
        // Artisan::call('migrate:refresh');
    }


    public function testCompanyIsCreatedSuccessfully()
    {
        User::factory()->count(1)->create();

        $user = User::first();
        $this->actingAs($user, 'web');

        $payload = [
            'name' => $this->faker->firstName,
            'website'  => "https://mailtrap.com",
            'email'      => $this->faker->safeEmail,

        ];



        $this->json('post', 'api/companies', $payload)
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'name',
                        'email',
                        'created_at',

                    ]
                ]
            );
        $this->assertDatabaseHas('companies', $payload);
    }


    public function testCompanyIsShownCorrectly()
    {
        User::factory()->count(1)->create();

        $user = User::first();
        $this->actingAs($user, 'web');

        $company = Company::create(
            [
                'name' => $this->faker->company,
                'website'  => $this->faker->freeEmailDomain,
                'email'      => $this->faker->email,
            ]
        );


        $this->json('get', "api/companies/" . $company->id)
            ->assertStatus(200)
            ->assertJson(
                [
                    'data' => [
                        'id'         => $company->id,
                        'name'       => $company->name,
                        'website'    => $company->website,
                        'email'      => $company->email,

                    ]
                ]
            );
    }

    public function testUpdateCompanyReturnsCorrectData()
    {

        User::factory()->count(1)->create();
        $user = User::first();
        $this->actingAs($user, 'web');

        $company = Company::create(
            [
                'name' => $this->faker->firstName,
                'website'  => "https://mailtrap.com",
                'email'      => $this->faker->safeEmail,
            ]
        );


        $payload = [
            'name' => $this->faker->firstName,
            'website'  => "https://mailtrap202.com",
            'email'      => $this->faker->safeEmail,

        ];


        $this->json('put', "api/companies/$company->id", $payload)
            ->assertStatus(200)
            ->assertJson(
                [
                    'data' => [
                        'id'         => $company->id,
                        'name'      => $payload['name'],
                        'website'  => $payload['website'],
                        'email'      => $payload['email'],

                    ]
                ]
            );
    }

    public function testCompanyIsDestroyed()
    {

        User::factory()->count(1)->create();
        $user = User::first();
        $this->actingAs($user, 'web');

        $data =
            [
                'name' => $this->faker->firstName,
                'website'  => "https://mailtrap.com",
                'email'      => $this->faker->safeEmail,
            ];

        $company = Company::create(
            $data
        );

        $this->json('delete', "api/companies/$company->id")
            ->assertNoContent();
        $this->assertDatabaseMissing('companies', $data);
    }
}
