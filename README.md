# CareMaster Project


# Laravel Breeze - Next.js Edition 🏝️
This project was built was Laravel Breeze(api) and Next.js(frontend)
- Laravel Breeze does not use Fortify


# Installation Instructions
- Clone repo
- navigate to api folder and run `composer install` if necessary
- run `php artisan serve`
- cd to frontend and run `npm install`
- run `npm run dev`
- app will run on `http://localhost:3000/`

# Notes
- copy `.env.example` to `.env` and make the following changes
- change `FILESYSTEM_DISK` to `public` in .env file so pictures can be copied from storage to public folder
- mailtrap credentials needs to be added to .env file so emails can be sent
- after creating new company, site will take a second or two to send email, in production environment I would use queues
- an additional factory (Employee Factory) was created to facilitate a smoother project
- I can be reached at jaycarter891@gmail.com for any questions
